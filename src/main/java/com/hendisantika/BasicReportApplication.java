package com.hendisantika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * DAO class which retrieves reports data populated in the report's chart
 * Created by IntelliJ IDEA.
 * Project : basic-report
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/18
 * Time: 20.42
 * To change this template use File | Settings | File Templates.
 */

@SpringBootApplication
public class BasicReportApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicReportApplication.class, args);
    }
}
