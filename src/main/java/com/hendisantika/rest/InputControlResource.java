package com.hendisantika.rest;

import com.hendisantika.dao.InputControlDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller which retrieves input controls data.
 * Created by IntelliJ IDEA.
 * Project : basic-report
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/18
 * Time: 20.59
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/input-control")
public class InputControlResource {

    private final InputControlDAO inputControlDAO;

    @GetMapping(value = "/data")
    public ResponseEntity<Object> getData() {
        return ResponseEntity.ok(inputControlDAO.getData());
    }
}
