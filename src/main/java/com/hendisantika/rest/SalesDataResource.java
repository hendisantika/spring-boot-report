package com.hendisantika.rest;

import com.hendisantika.dao.SalesDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * REST Controller which retrieves data used in the chart.
 * Created by IntelliJ IDEA.
 * Project : basic-report
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/18
 * Time: 21.02
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/sales")
public class SalesDataResource {

    private final SalesDAO salesDAO;

    @GetMapping(value = "/data")
    public ResponseEntity<Object> getData(@RequestParam Integer carManufacturerId) {
        return ResponseEntity.ok(salesDAO.getData(carManufacturerId));
    }
}
