package com.hendisantika.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : basic-report
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/18
 * Time: 20.57
 * To change this template use File | Settings | File Templates.
 */

@Getter
@AllArgsConstructor
public class InputControlDTO {

    private String text;
    private Integer value;

}
