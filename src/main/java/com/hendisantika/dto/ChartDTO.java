package com.hendisantika.dto;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * DAO class which retrieves reports data populated in the report's chart
 * Created by IntelliJ IDEA.
 * Project : basic-report
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/18
 * Time: 20.55
 * To change this template use File | Settings | File Templates.
 */
@Getter
public class ChartDTO {

    private List<String> xAxis;
    private List<Double> yAxis;

    public void addXaxisValue(String value) {
        if (xAxis == null) {
            xAxis = new ArrayList<>();
            xAxis.add(value);
        } else {
            xAxis.add(value);
        }
    }

    public void addYaxisValue(Double value) {
        if (yAxis == null) {
            yAxis = new ArrayList<>();
            yAxis.add(value);
        } else {
            yAxis.add(value);
        }
    }
}
