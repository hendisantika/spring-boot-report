package com.hendisantika.dao;

import com.hendisantika.dto.InputControlDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO class which retrieves data populated in the input controls of the report
 * Created by IntelliJ IDEA.
 * Project : basic-report
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/02/18
 * Time: 20.49
 * To change this template use File | Settings | File Templates.
 */
@Repository
@RequiredArgsConstructor
public class InputControlDAO {

    private static final String TEXT_COLUMN_NAME = "TEXT2";
    private static final String VALUE_COLUMN_NAME = "VALUE2";

    private final JdbcTemplate jdbcTemplate;

    public List<InputControlDTO> getData() {
        return jdbcTemplate.query(getQuery(), new ResultSetExtractor<List<InputControlDTO>>() {
            @Override
            public List<InputControlDTO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<InputControlDTO> inputControlDTOs = new ArrayList<>();

                while (resultSet.next()) {
                    inputControlDTOs.add(new InputControlDTO(resultSet.getString(TEXT_COLUMN_NAME), resultSet.getInt(VALUE_COLUMN_NAME)));
                }
                return inputControlDTOs;
            }
        });
    }

    private String getQuery() {
        return "SELECT ID AS VALUE2, NAME AS TEXT2 FROM CAR_MANUFACTURER ORDER BY ID";
    }
}
