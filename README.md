# Spring Boot Report

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-report.git`
2. Navigate to the folder: `cd spring-boot-report`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser:
5. Home Page: http://localhost:8080/
6. Sales Data: http://localhost:8080/sales/data?carManufacturerId=2
7. Input Data: http://localhost:8080/input-control/data
